﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
namespace DR_APPOINTMENTS_API.Classes
{
    public class DBInjector
    {
        internal string strConnstionResult = "";
        private readonly SqlConnection conn;
        public DBInjector()
        {
            string conString = "Server=Rahul;Database=DOCTOR_APPOINTMENT;Trusted_Connection=True;";
            conn = new SqlConnection(conString);
            conn.Open();
            if (conn.State == ConnectionState.Open)
            {
                strConnstionResult = "Connected :)";
                conn.Close();
            }
            else if (conn.State == ConnectionState.Closed)
            {
                strConnstionResult = "Connected :)";
            }
            else if (conn.State == ConnectionState.Broken)
            {
                strConnstionResult = "Something Happened bad, Not Connected :)";
                conn.Close();
            }
        }

        public DataTable GetDataInDT(string query)
        {
            DataTable dataTable = new DataTable();
            conn.Open();
            SqlDataAdapter sqlData = new SqlDataAdapter(query, conn);
            sqlData.Fill(dataTable);
            conn.Close();
            return dataTable;
        }
    }
}
