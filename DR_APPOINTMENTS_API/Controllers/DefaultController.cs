﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DR_APPOINTMENTS_API.Classes;
using DR_APPOINTMENTS_API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DR_APPOINTMENTS_API.Controllers
{
    [Route("/")]
    [ApiController]
    [Consumes("application/x-www-form-urlencoded")]
    public class DefaultController : ControllerBase
    {
        private readonly DBInjector dBInjector = new DBInjector();

        private readonly ILogger logger;
        public DefaultController(ILoggerFactory factory)
        {
            logger = factory.CreateLogger<DefaultController>();
        }

        // GET api/values
        [HttpGet]
        [Route("check")]
        public IActionResult ServerCheck()
        {
            var result = dBInjector.strConnstionResult;
            if (!string.IsNullOrEmpty(result))
            {
                return Ok(result);
            }
            else
            {
                return Ok("Not Connected ):");
            }
        }

        // GET api/values/5
        [HttpPost]
        [Route("login")]
        public IActionResult VerifyLogin(string loginJson)
        {
            try
            {
                var loginModel = JsonConvert.DeserializeObject<LoginViewModel>(loginJson);
                string query = "SELECT COUNT(*) FROM USER_INFO WHERE USERNAME='" + loginModel.Username + "' AND PASSWORD='" + loginModel.Password + "' " +
                "AND USER_TYPE='" + loginModel.UserType + "' ";
                var dt = dBInjector.GetDataInDT(query);
                return Ok(dt.Rows.Count.ToString());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
